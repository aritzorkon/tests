<?php
$token = "123456";

if (!isset($_SERVER['HTTP_X_AUTH_TOKEN'])) {
	die("Authentification token error");
}

if ($_SERVER['HTTP_X_AUTH_TOKEN'] != $token) {
	die("Authentification token error");
}

header('content-type:application/json');

$data = json_encode([
	"pinpad"=>"*",
	"param2"=>true,
	"param3"=> [
		"param3.1" => "nombre",
		"param3.2" => 1005
	]
]);

echo $data;
