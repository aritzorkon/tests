<?php
$ch = curl_init("http://localhost/testAPI-JSONbody/enviar.php");

curl_setopt_array($ch, [
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_HTTPHEADER     => array("x-auth-token: 123456"),
]);
$result = curl_exec($ch);
curl_close($ch);

$result = json_decode($result, true);

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <title>
            Recepción
        </title>
         <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined" rel="stylesheet">

        <style>

			ul {
			    list-style: none;
			    width: 50%;
			    font-size: 100%;
			}
			li {
				padding: 0;
				margin: 0;
				border: 1px solid #acacac;
				background: #ececec;
			}
			b {
			    width: 45%;
			    padding: 1vh 1vw;
			    display: inline-block;
			    border-right: 1px solid #c4c4c4;
			}
			span {
			    display: inline-block;
			    padding: 1vh 1vw;
			    width: 45%;
			    text-align: center;
			}
			ul>li>ul {
				margin: 1vh 2% 1vh 2%;
				padding: 0;background: white;
				width: 96%;
			}
			ul>li>ul>li {
				border: none;
				border-bottom: 1px solid lightgray;
				background: none;
			}
			ul>li>ul>li>span, ul>li>ul>li>b {
				border: none;
			}
			.expand-close {
				text-align: left;
				width: 100%;
				padding: 0;
			}
			.expand-close>.bt-expand-close-class {
				cursor: pointer;
				line-height: 0;
				transform: translateY(7px);
			}

        </style>
    </head>
    <body>
    	<script  type="text/javascript">
		    function show (cont) {
	    		cont.style.display = "block";

	    		var fontSize = 0;
	    		id = setInterval(function() {
	    			if (fontSize == 100) {
	    				clearInterval(id);
	    			} else {
	    				fontSize += 10;
	    			}
	    			cont.style.fontSize = fontSize +"%";
	    		}, 2);
	    	}
	    	function hide (cont) {
	    		var fontSize = 100;
	    		id = setInterval(function() {
	    			if (fontSize === 0) {
	    				clearInterval(id);
	    				cont.style.display = "none";
	    			} else {
	    				fontSize -= 10;
	    			}
	    			cont.style.fontSize = fontSize +"%";
	    		}, 2);
	    	}
    	</script>
        <h1>
            Tabla de datos
        </h1>
        <ul>
            <li>
                <b>
                    Pinpad
                </b>
                <span>
                    <?php echo $result['pinpad'] ?>
                </span>
            </li>
            <li>
                <b>
                    Param 2
                </b>
                <span>
                    <?php echo $result['param2'] ?>
                </span>
            </li>
            <li>
            	<span class="expand-close">
	                <b style="border: none">
	                    Param 3
	                </b>
	                <span id="bt-expand-close" class="material-icons-outlined bt-expand-close-class">
	                	chevron_right
	            	</span>
                </span>
                <ul id="expandable" style="display: none;">
                    <li>
                        <b>
                            Param 3.1
                        </b>
                        <span>
                            <?php echo $result['param3']['param3.1'] ?>
                        </span>
                    </li>
                    <li style="border: none;">
                        <b>
                            Param 3.2
                        </b>
                        <span>
                            <?php echo $result['param3']['param3.2'] ?>
                        </span>
                    </li>
                </ul>
           		<script type="text/javascript">
	            	document.getElementById("bt-expand-close").onclick = function() {
	            		var expandable = document.getElementById("expandable");

	            		if (expandable.style.display == "block") {
	            			hide(expandable);
	            			this.innerHTML = "chevron_right";
	            		} else {
	            			show(expandable);
	            			this.innerHTML = "expand_more";
	            		}
	            	};
                </script>
            </li>
            <li>
            	<b>Param 4</b>
            	<span>null</span>
            </li>
        </ul>
    </body>
</html>