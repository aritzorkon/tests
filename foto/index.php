<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>form</title>
	<style>
		input[type="file"]{
			width: 75%;
			text-align: center;
			margin: auto;
			display: block;
			box-shadow: 0 0 3px 0 gray;
			border-radius: 4px;
			padding: 2vh 0vw;
			font-size: 16px;
		}
		input[type="file"]:-moz-drag-over{
			background: lightgrey;
		}
		p, h1 {
			display: block;
			width: 90%;
			margin: 5vh auto;
		}
		#foto_upload, #limpiar-datos {
			font-size: 16px;
			width: 15%;
			padding: 0.4% 0;
			background: #0064ff;
			color: white;
			border: none;
			box-shadow: 0 0 4px 0 rgba(0,0,0,0.4);
			display: block;
			margin: auto;
			cursor: pointer;
		}
		#limpiar-datos {
			background: #9b1818;
			margin-bottom: 20px;
		}
		#foto_upload:hover, , #limpiar-datos:hover {
			filter: brightness(80%);
		}
		img {
			box-shadow: 0 0 4px 0 rgba(0,0,0,0.6);
			display: block;
			margin: 2vh auto;
			width: 20vw;
			height: 30vh;
		}
	</style>
</head>
<body>
	<h1>Sube una imagen</h1>
	<form action="control.php" method="POST" enctype="multipart/form-data">
		<input type="file" name="foto" id="foto" accept="image/x-png, image/jpeg, image/jpg" size="2000000" required="">
		<p>Tipos admitidos: jpeg, jpg, png</p>
		<img src="" alt="" id="foto-preview">
		<button type="reset" id="limpiar-datos">Limpiar</button>
		<button type="submit" id="foto_upload" name="foto_upload">Enviar</button>
	</form>
	<script type="text/javascript">
		var img = document.getElementById("foto-preview");
		
		document.getElementById("foto").onchange = function(e) {
			img.src = URL.createObjectURL(this.files[0]);
		};
		document.getElementById("limpiar-datos").addEventListener("click", function (e) {
			img.src = "";
		});
	</script>
</body>
</html>
