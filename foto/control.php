<?php
if (isset($_POST['foto_upload'])) {

    $nomArchivo = $_FILES['foto']['name'];

    if ((strpos($nomArchivo, "jpeg") || strpos($nomArchivo, "jpg") || strpos($nomArchivo, "png")) && 
    	$_FILES['foto']['size'] < 2000000) {

        rename($_FILES['foto']['tmp_name'], $nomArchivo);

        $contenidoBinario = file_get_contents($nomArchivo);
        $imgBase64 = base64_encode($contenidoBinario);

        unlink($nomArchivo);

        echo "<img src='data:".$_FILES["foto"]["type"].";base64,$imgBase64' style='max-width:100%; max-height: 100vh;'>";

    } else {
        echo "no es válido";
    }
}
