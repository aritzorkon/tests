<?php

for ($i = 0; $i < 5; $i++) {
    $randArray[] = rand(1, 5);
}

echo "<p>Números iniciales</p>";
var_dump($randArray);

for ($i = 1; $i < count($randArray); $i++) {
	echo "<p>Comprobando número $i</p>";
	for ($j=0; $j < $i; $j++) { 
		if ($randArray[$j] == $randArray[$i]) {
			$randArray[$i] = rand(1, 5);
			$j = -1;
			var_dump($randArray);
		}
	}
}

//ordenar
for ($i = 1; $i < count($randArray); $i++) {
    if ($randArray[$i] < $randArray[$i - 1]) {
        $aux               = $randArray[$i - 1];
        $randArray[$i - 1] = $randArray[$i];
        $randArray[$i]     = $aux;
        $i                 = 0;
    }
}

echo "Números ordenados";
var_dump($randArray);
